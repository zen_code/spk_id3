<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nama' class='col-sm-4 col-xs-12 control-label'>No Peserta PKH</label>
        <div class='col-sm-8 col-xs-12'>
            <select name="no_peserta" class="form-control">
                @foreach($peserta as $item)
                    <option value="{{ $item->no_peserta }}" @if(!is_null($data)) @if($item->no_peserta == $data->no_peserta) selected="selected" @endif @endif>{{ $item->no_peserta}} - {{ $item->name}} - {{ $item->nik}}</option>
                @endforeach
            </select>
        </div>    
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-4 col-xs-12 control-label'>Dinding Rumah</label>
        <div class='col-sm-8 col-xs-12'>
            <select name="dinding_rumah" class="form-control">
                <option <?php if($data->dinding_rumah =='Bambu') {echo "selected";}?> value="Bambu">Bambu</option>
                <option <?php if($data->dinding_rumah =='Tembok') {echo "selected";}?> value="Tembok">Tembok</option>
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-4 col-xs-12 control-label'>Lantai Rumah</label>
        <div class='col-sm-8 col-xs-12'>
            <select name="lantai_rumah" class="form-control">
                <option <?php if($data->lantai_rumah =='Tanah') {echo "selected";}?> value="Tanah">Tanah</option>
                <option <?php if($data->lantai_rumah =='Teraso') {echo "selected";}?> value="Teraso">Teraso</option>
                <option <?php if($data->lantai_rumah =='Keramik') {echo "selected";}?> value="Keramik">Keramik</option>
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-4 col-xs-12 control-label'>Pekerjaan</label>
        <div class='col-sm-8 col-xs-12'>
            <select name="pekerjaan" class="form-control">
                <option <?php if($data->pekerjaan =='Buruh Tani') {echo "selected";}?> value="Buruh Tani">Buruh Tani</option>
                <option <?php if($data->pekerjaan =='Petani') {echo "selected";}?> value="Petani">Petani</option>
                <option <?php if($data->pekerjaan =='Pedagang') {echo "selected";}?> value="Pedagang">Pedagang</option>
                <option <?php if($data->pekerjaan =='Belum/Tidak Bekerja') {echo "selected";}?> value="Belum/Tidak Bekerja">Belum/Tidak Bekerja</option>
                <option <?php if($data->pekerjaan =='PNS/TNI POLRI') {echo "selected";}?> value="PNS/TNI POLRI">PNS/TNI POLRI</option>
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-4 col-xs-12 control-label'>Kepemilikan Balita Ibu Hamil</label>
        <div class='col-sm-8 col-xs-12'>
            <select name="balita_ibuhamil" class="form-control">
                <option <?php if($data->balita_ibuhamil =='Ya') {echo "selected";}?> value="Ya">Ya</option>
                <option <?php if($data->balita_ibuhamil =='Tidak') {echo "selected";}?> value="Tidak">Tidak</option>
            </select>
        </div>  
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-4 col-xs-12 control-label'>Kepemilikan Anak Sekolah</label>
        <div class='col-sm-8 col-xs-12'>
            <select name="anak_sekolah" class="form-control">
                <option <?php if($data->anak_sekolah =='Ya') {echo "selected";}?> value="Ya">Ya</option>
                <option <?php if($data->anak_sekolah =='Tidak') {echo "selected";}?> value="Tidak">Tidak</option>
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-4 col-xs-12 control-label'>Kepemilikan Lansia / disabilitas</label>
        <div class='col-sm-8 col-xs-12'>
            <select name="lansia_disabilitas" class="form-control">
                <option <?php if($data->lansia_disabilitas =='Ya') {echo "selected";}?> value="Ya">Ya</option>
                <option <?php if($data->lansia_disabilitas =='Tidak') {echo "selected";}?> value="Tidak">Tidak</option>
            </select>
        </div>
    </div>

    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/data-latih/save', data, '#result-form-konten');
        })
    })
</script>
