<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nama' class='col-sm-3 col-xs-12 control-label'>No Peserta PKH</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="no_peserta" class="form-control" value="{{$data->no_peserta}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-3 col-xs-12 control-label'>Nama</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="name" class="form-control" value="{{$data->name}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-3 col-xs-12 control-label'>Tahun</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="tahun" class="form-control" value="{{$data->tahun}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-3 col-xs-12 control-label'>NIK</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nik" class="form-control" value="{{$data->nik}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-3 col-xs-12 control-label'>Tanggal Lahir</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="tanggal_lahir" class="form-control" value="{{$data->tanggal_lahir}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-3 col-xs-12 control-label'>Alamat</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="alamat" class="form-control" value="{{$data->alamat}}" required="">
        </div>
    </div>

    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/identitas-peserta/save', data, '#result-form-konten');
        })
    })
</script>
