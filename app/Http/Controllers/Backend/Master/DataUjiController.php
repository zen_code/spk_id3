<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\IdentitasPeserta;
use App\Models\DataUji;
use Illuminate\Http\Request;

class DataUjiController extends Controller
{
    public  function index(){

        $data=DataUji::all();
        $params=[
            'data'=>$data,
            'title'=>'Data Latih'
        ];
        return view('backend.master.data-uji.index',$params);
    }

    public  function form(Request $request){
        $id = $request->input('id');
        $peserta = IdentitasPeserta::all();
        if($id){
            $data = DataUji::find($id);
        }else{
            $data = new DataUji();
        }
        $params = [
            'title' => 'Identitas Peserta',
            'data' => $data,
            'peserta' => $peserta,
        ];
        return view('backend.master.data-uji.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = DataUji::find($id);
        }else{
            $data = new DataUji();
            $cek=DataUji::where(['no_peserta' => $data->no_peserta])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! No Peserta Sudah Terdaftar!</div>";
            }
        }
        $data->no_peserta = $request->no_peserta;
        $data->dinding_rumah = $request->dinding_rumah;
        $data->lantai_rumah = $request->lantai_rumah;
        $data->pekerjaan = $request->pekerjaan;
        $data->balita_ibuhamil = $request->balita_ibuhamil;
        $data->anak_sekolah = $request->anak_sekolah;
        $data->lansia_disabilitas = $request->lansia_disabilitas;
        $data->kelas_asli = $request->kelas_asli;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data Uji berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data Uji gagal disimpan!</div>";
        }

    }
    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            DataUji::find($id)->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal dihapus!</div>";
        }

    }

}