<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\IdentitasPeserta;
use Illuminate\Http\Request;

class IdentitasPesertaController extends Controller
{
    public  function index(){

        $data=IdentitasPeserta::all();
        $params=[
            'data'=>$data,
            'title'=>'Identitas Peserta'
        ];

        return view('backend.master.identitas-peserta.index',$params);

    }

    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = IdentitasPeserta::find($id);
        }else{
            $data = new IdentitasPeserta();
        }
        $params = [
            'title' => 'Identitas Peserta',
            'data' => $data
        ];
        return view('backend.master.identitas-peserta.form',$params);
    }
    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = IdentitasPeserta::find($id);
        }else{
            $data = new IdentitasPeserta();
            $cek=IdentitasPeserta::where(['nik' => $data->nik])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! No NIK Sudah Terdaftar!</div>";
            }
        }
        $data->no_peserta = $request->no_peserta;
        $data->tahun = $request->tahun;
        $data->name = $request->name;
        $data->nik = $request->nik;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->alamat = $request->alamat;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Peserta berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            dd($ex);
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peserta gagal disimpan!</div>";
        }

    }
    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            IdentitasPeserta::find($id)->delete();
            return "
            <div class='alert alert-success'>Peserta berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Peserta gagal dihapus!</div>";
        }

    }

}