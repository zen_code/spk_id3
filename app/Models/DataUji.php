<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DataUji extends Model
{
    protected $table = 'data_uji';
    protected $primaryKey = 'id';
    public $timestamps = false;
}