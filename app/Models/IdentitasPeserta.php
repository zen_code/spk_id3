<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class IdentitasPeserta extends Model
{
    protected $table = 'identitas';
    protected $primaryKey = 'id';
    public $timestamps = false;
}