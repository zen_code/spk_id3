
## Cara Instalasi


- Buat folder **spk** pada folder htdocs anda
- Masukkan file aplikasi ini ke dalamnya
- Jalankan **composer install** atau **composer update**
- Setelah selesai,buat database dan beri nama spk di phpmyadmin anda
- Kemudian import file **spk.sql**  yang ada di folder **db** ke phpmyadmin
- Setting **.env** anda sesuai dengan konfigurasi phpmyadmin anda
- Dan setting **APP_URL** sesuai nama aplikasinya


## Untuk Testing
- username : **admin**
- password : **admin**

Sekian dan terima kasih


