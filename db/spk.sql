-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Mar 2020 pada 15.17
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_latih`
--

CREATE TABLE `data_latih` (
  `id` int(10) NOT NULL,
  `no_peserta` varchar(100) NOT NULL,
  `dinding_rumah` varchar(100) NOT NULL,
  `lantai_rumah` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `balita_ibuhamil` varchar(100) NOT NULL,
  `anak_sekolah` varchar(100) NOT NULL,
  `lansia_disabilitas` varchar(100) NOT NULL,
  `kelas_asli` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_latih`
--

INSERT INTO `data_latih` (`id`, `no_peserta`, `dinding_rumah`, `lantai_rumah`, `pekerjaan`, `balita_ibuhamil`, `anak_sekolah`, `lansia_disabilitas`, `kelas_asli`, `created_at`, `updated_at`) VALUES
(1, '12345', 'Bambu', 'Tanah', 'Petani', 'Tidak', 'Tidak', 'Tidak', NULL, '2020-03-20 07:39:27', '2020-03-20 14:38:05'),
(2, '1234567', 'Tembok', 'Keramik', 'Buruh Tani', 'Ya', 'Ya', 'Ya', NULL, '2020-03-20 07:41:39', '2020-03-20 14:41:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_uji`
--

CREATE TABLE `data_uji` (
  `id` int(10) NOT NULL,
  `no_peserta` varchar(100) NOT NULL,
  `dinding_rumah` varchar(100) NOT NULL,
  `lantai_rumah` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `balita_ibuhamil` varchar(100) NOT NULL,
  `anak_sekolah` varchar(100) NOT NULL,
  `lansia_disabilitas` varchar(100) NOT NULL,
  `kelas_asli` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_uji`
--

INSERT INTO `data_uji` (`id`, `no_peserta`, `dinding_rumah`, `lantai_rumah`, `pekerjaan`, `balita_ibuhamil`, `anak_sekolah`, `lansia_disabilitas`, `kelas_asli`, `created_at`, `updated_at`) VALUES
(1, '12345', 'Bambu', 'Tanah', 'Petani', 'Tidak', 'Tidak', 'Tidak', NULL, '2020-03-20 07:39:27', '2020-03-20 14:38:05'),
(2, '1234567', 'Tembok', 'Keramik', 'Buruh Tani', 'Ya', 'Ya', 'Ya', NULL, '2020-03-20 07:41:39', '2020-03-20 14:41:31'),
(3, '1234567', 'Bambu', 'Tanah', 'Buruh Tani', 'Ya', 'Ya', 'Ya', NULL, '2020-03-20 07:55:55', '2020-03-20 14:55:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `identitas`
--

CREATE TABLE `identitas` (
  `id` int(10) NOT NULL,
  `no_peserta` varchar(100) NOT NULL,
  `tahun` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `nik` int(100) NOT NULL,
  `tanggal_lahir` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `identitas`
--

INSERT INTO `identitas` (`id`, `no_peserta`, `tahun`, `name`, `nik`, `tanggal_lahir`, `alamat`, `created_at`, `updated_at`) VALUES
(2, '1234567', 2020, 'Muhamat Zaenal Mahmut', 2103161040, '11 Juli 1997', 'Jl. Ayam Alas No. 24 Mantup Lamongan', '2020-03-20 04:39:23', '2020-03-20 11:39:05'),
(3, '12345', 2016, 'Amalia', 1234567, '11 Juli 1997', 'Lamongan', '2020-03-20 06:40:58', '2020-03-20 13:40:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `password`) VALUES
(1, 'Zumrotus Sholikah', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_latih`
--
ALTER TABLE `data_latih`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_uji`
--
ALTER TABLE `data_uji`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_latih`
--
ALTER TABLE `data_latih`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `data_uji`
--
ALTER TABLE `data_uji`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `identitas`
--
ALTER TABLE `identitas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
